In order to use Video Download Helper addon with firefox with apparmor in enforce mode it's necessary to make some small changes to firefox's apparmor profile.
You can find firefox's apparmor profile at /etc/apparmor.d/

cd /etc/apparmor.d/

Then edit its profile with:

sudo nano usr.bin.firefox

And find the lines:

  # allow access to documentation and other files the user may want to look
  # at in /usr and /opt
  /usr/ r,
  /usr/** r,
  /opt/ r,
  /opt/** r,

And add the following down below:

  /opt/net.downloadhelper.coapp/ ixr,
  /opt/net.downloadhelper.coapp/* ixr,
  /opt/net.downloadhelper.coapp/** ixr,
  /media/sf_Web_Browser/ rw,
  /media/sf_Web_Browser/* rw,
  /media/sf_Web_Browser/** rw,

So that it looks like this:

  # allow access to documentation and other files the user may want to look
  # at in /usr and /opt
  /usr/ r,
  /usr/** r,
  /opt/ r,
  /opt/** r,
  # added by me
  /opt/net.downloadhelper.coapp/ ixr,
  /opt/net.downloadhelper.coapp/* ixr,
  /opt/net.downloadhelper.coapp/** ixr,
  /media/sf_Web_Browser/ rw,
  /media/sf_Web_Browser/* rw,
  /media/sf_Web_Browser/** rw,
