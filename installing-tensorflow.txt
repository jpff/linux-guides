Install CUDA Dev Kit

Install Python 64bit version, include pip3 during installation, MUST BE 64BIT VERSION

Install TensorFlow with: 

pip3 install --upgrade tensorflow
pip3 install --upgrade tensorflow-gpu

If validating TensorFlow's installation with the following python commands:

>>> import tensorflow as tf
>>> hello = tf.constant('Hello, TensorFlow!')
>>> sess = tf.Session()
>>> print(sess.run(hello))


fails then execute following command in the terminal:
pip install tensorflow --upgrade --force-reinstall

Should have a functional installation now
